---
title: "Tutorium Statistik 1"
subtitle: "Übungsaufgabe Messtheorie"
author: "Florian Kohrt"
date: "7. Mai 2019"
always_allow_html: yes
output:
  revealjs::revealjs_presentation:
    theme: serif
    transition: slide
    center: true
    reveal_plugins: ["zoom", "notes"]
    self_contained: false
    smart: false
    reveal_options:
      showNotes: true
      previewLinks: false
      width: 1600
      height: 900
      navigationMode: linear
    css: ../styles.css
  pagedown::html_paged: 
    toc: false
    number_sections: false
  tint::tintPdf: default
---

# **1.** Gedächtnisexperiment

> Die Ergebnisse eines Gedächtnisexperiments zur retroaktiven Hemmung werden in %-korrekt erinnerte Items angegeben.

<h2 class="fragment">**1.1** Welches Skalenniveau haben diese Daten (%korrekt)?</h2>

> - Ordnung (mehr vs. weniger Prozent) $\rightarrow$ Ordinalskala
> - Differenz sinnvoll interpretierbar $\rightarrow$ Intervallskala
> - Vielfache (z. B. „doppelt so gut“) sinnvoll $\rightarrow$ Verhältnisskala
> - wäre auch in Promille formulierbar (Einheit nicht natürlich) $\rightarrow$ keine Absolutskala

<h2 class="fragment">**1.2** Geben Sie ein Beispiel für eine zulässige Transformation dieser Daten</h2>

## **1.2** Zulässige Transformationen

> - Änderung der Einheit: Multiplikation mit einer Konstanten
> - Umrechnen in Dezimalbrüche
> - Umrechnen in Promille

# **2.** On the statistical treatment of football numbers

> In einem klassischen (ironisch gemeinten) Artikel zur Frage zulässiger statistischer Berechnungen bei bestimmten Skalenniveaus argumentiert Lord (1953), daß für statistische Auswertungen das Skalenniveau der Daten keine Rolle spiele: „The numbers don’t know where they come from“. In der Argumentation benutzt er das Beispiel sogenannter football numbers, der Trikotnummern einer Mannschaft. (Für NichtFootballer: Die Trikotnummern dienen nur zur Unterscheidung der Spieler, sind also weder mit der Spielposition noch mit dem Dienstalter, der Spielstärke o.ä. verknüpft.)

<h2 class="fragment">**2.1** Welches Skalenniveau haben football numbers lt. erstem Absatz?</h2>

## **2.** (zweiter Absatz)

nominalskaliert

<blockquote class="fragment">Im Beispiel im Artikel beschwert sich eine Junior-Footballmannschaft, daß ihr bei der Ausgabe der Trikots kleinere Nummern zugeteilt würden als der Senior-Mannschaft. Zum Test dieser Beschwerde berechnet der Football-Statistiker Mittelwerte und Streuungen der Trikotnummern der beiden Mannschaften und folgert (auf Basis des sog. t-Tests, lernen Sie in QMB), daß tatsächlich die Juniormannschaft kleinere Nummern erhalten habe.</blockquote>

<h2 class="fragment">**2.2** Darf der Statistiker im beschriebenen Fall Mittelwerte / Streuungen berechnen?</h2>

<div class="fragment">Ja, da die Berechnung nur der Ermittlung einer Eigenschaft des numerischen Relativs dient.</div>

## **2.** (dritter Absatz)

> Auf den Einwurf eines Psychologie-Professors, das seien doch nur football-numbers, er dürfe keine Mittelwerte berechnen, zuckt der Statistiker mit den Schultern und sagt „The numbers don’t know where they come from“; Hauptsache, die Verteilungs- und Unabhängigkeitsvoraussetzungen des t-Tests seien erfüllt. Der Professor, der vorher seine Fragebögen/Ratings nur im stillen Kämmerlein gemittelt hat, lässt sich überzeugen, geht hin und tut in Zukunft desgleichen.

<h2 class="fragment">**2.3** Finden Sie die Argumentation von Lord schlüssig (und ein gutes Beispiel für Skalenniveaus)? Begründen Sie Ihre Antwort.</h2>

<div class="fragment">Kein gutes Beispiel, da es von einem unproblematischen Beispiel (Frage bezieht sich auf numerisches Relativ) unzulässig auf alle Fälle verallgemeinert (damit auch auf Fragen, die sich auf empirisches Relativ beziehen).</div>

# **3.** Beispiele für Intervallskala

> In einem Lehrbuch zur Statistik (Zöfel, 2003) werden im Kapitel “Das Messen” die Variablen Geschlecht, Alter, Familienstand, Schulbildung, Beruf, Körpergewicht und Rauchgewohnheit (Nichtraucher, mäßig, stark, sehr stark) eingeführt und anschließend die Skalenniveaus anhand dieser Variablen erklärt.
> 
> Wie Sie wissen, sind die Skalenniveaus nach Informationsgehalt geordnet.

<h2 class="fragment">**3.1** Welches (maximale) Skalenniveau haben diese Variablen (genauer: Zahlen, die den Werten der Variablen zugeordnet werden)?</h2>

## **3.1** Skalenniveaus

> - Geschlecht: <span class="fragment">nominalskaliert</span>
> - Alter: <span class="fragment">verhältnisskaliert</span>
> - Familienstand: <span class="fragment">nominalskaliert</span>
> - Schulbildung: <span class="fragment">ordinalskaliert</span>
> - Beruf: <span class="fragment">nominalskaliert</span>
> - Körpergewicht: <span class="fragment">verhältnisskaliert</span>
> - Rauchgewohnheit: <span class="fragment">ordinalskaliert</span>

## **3.** (Fortsetzung)

> Auf Seite 19 findet man folgende Tabelle der Skalenniveaus:

Skalenniveau|empirische Relevanz
---|---
Nominal|keine
Ordinal|Ordnung der Zahlen
Intervall|Differenzen der Zahlen
Verhältnis|Verhältnisse der Zahlen

<h2 class="fragment">**3.2** Beim Durchlesen der Tabelle ist Herr Scheuchenpflug zusammengezuckt (und Sie sollten es auch). Warum (und nein, es liegt nicht am fehlenden Absolutniveau)?</h2>

> - auch eine Messung auf nominalniveau hat empirische Relevanz, siehe kommende Europawahl
> - außerdem ist nicht die Ordnung der Zahlen empirisch relevant, sondern die Ordnung der Attributsausprägungen im empirischen Relativ

## **3.3** Diskutieren Sie jeden einzelnen Satz hinsichtlich folgender Aspekte: Ist die Aussage korrekt? Ist das ein gutes Beispiel für Intervallniveau? Wären wir derselben Meinung? Und vielleicht: Ist der Satz in klarer Sprache geschrieben?

<blockquote>Bezüglich des Körpergewichts geben die entsprechenden Werte nicht nur eine Rangordnung der beteiligten Personen wieder, auch den Differenzen zweier Werte kommt eine empirische Bedeutung zu [1].</blockquote>

<div class="fragment">wahr</div>

<blockquote class="fragment">Hat etwa August ein Körpergewicht von 70 kg, Bertram eines von 80 kg und Christian ist 90 kg schwer, so kann man sagen, dass Bertram im Vergleich zu August um ebensoviel schwerer ist wie Christian im Vergleich zu Bertram (nämlich um 10 kg)[2].</blockquote>

<div class="fragment">wahr</div>

## **3.3** (erste Fortsetzung)

<blockquote>Solche Variablen, bei denen der Differenz (dem Intervall) zwischen zwei Werten eine empirische Bedeutung zukommt, nennt man intervallskaliert [3].</blockquote>

<div class="fragment">Das maximale Skalenniveau des Merkmals _Körpergewicht_ ist die Verhältnisskala und **nicht** die Intervallskala. Es ist daher irreführend, an ihm die Intervallskala zu erläutern.</div>

<blockquote class="fragment">Ihre Bearbeitung unterliegt keinen Einschränkungen; so ist zum Beispiel der Mittelwert ein sinnvoller statistischer Kennwert zur Beschreibung dieser Variablen[4].</blockquote>

> - Falsch, intervallskalierte Merkmale dürfen nicht beliebig transformiert werden: Die Transformationen ordinalskalierter oder nominalskalierter sind i. A. nicht zulässig.
> - Möglicherweise meint der Autor, dass viele Kennwerte bei intervallskalierten Merkmalen berechnet werden dürfen.

## **3.3** (zweite Fortsetzung)

<blockquote>Eine weitere intervallskalierte Variable im gegebenen Beispiel ist das Alter. [5]</blockquote>

<div class="fragment">Das maximale Skalenniveau des Merkmals _Alter_ ist ebenfalls die Verhältnisskala.</div>

<blockquote class="fragment">Der Übergang von Ordinal- zu Intervallniveau ist fließend und eine Einordnung in eines der beiden Niveaus erscheint manchmal durchaus strittig. [6]</blockquote>

<div class="fragment">Die Frage, ob Ordinal- oder Intervallskalierung vorliegt, ist nicht losgelöst diskutierbar, wie der Satz suggeriert, sondern ist empirisch und muss anhand des empirischen Relativs geklärt werden.</div>

## **3.3** (dritte Fortsetzung)

<blockquote>Während man beispielsweise die zwischen den Zahlen 1 und  6 vergebenen Schulnoten als ordinalskaliert ansieht, ist man bei den in der Oberstufe vergebenen Punktnoten von 0 bis 15 wohl eher geneigt, Intervallniveau anzunehmen [7].</blockquote>

> - Der Satz legt nahe, dass es von der Anzahl verwendeter Werte abhängt, welches Skalenniveau vorliegt. Das ist nicht der Fall.
> - Welches Skalenniveau Schulnoten haben, muss anhand des empirischen Relativs untersucht werden und ist i. A. ungeklärt.
> - In unserem Schulsystem werden sie behandelt, als ob sie intervallskaliert seien, da Notendurchschnitte berechnet werden.

# Ende

Quelle: Zöfel, P. (2003). Statistik für Psychologen im Klartext. München: Pearson Studium.

Diese Folien sind abrufbar unter [fkohrt.gitlab.io/tutorium-statistik-1-ss-19/](https://fkohrt.gitlab.io/tutorium-statistik-1-ss-19/)

Stand: `r format(Sys.time(),usetz=TRUE, tz='Europe/Berlin')`

<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/" class="img"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>