# Tutorium Statistik 1 SS 19

Material für ein Tutorium der Vorlesung Statistik 1 im Bachelor Psychologie an der Julius-Maximilians-Universität Würzburg

https://fkohrt.gitlab.io/tutorium-statistik-1-ss-19/

## Output Formats

- Slides
  - HTML: [reveal.js](https://github.com/hakimel/reveal.js)
    - `sed -i 's/<BASE>_files\/reveal\.js-3\.3\.0\.1/\.\.\/reveal\.js/g' <BASE>.html`
    - add `navigationMode: 'linear',` to the `Reveal.initialize()` call
  - PDF: append `?print-pdf&showNotes=false&controls=false&controlsTutorial=false&progress=false&slideNumber=true&pdfSeparateFragments=false&fragments=false` to the query string and print in Chromium
- Handout
  - HTML: [pagedown](https://github.com/rstudio/pagedown)
    - `sed -i 's/<BASE>_files\/paged-0\.2/\.\.\/pagedown\/inst\/resources/g' <BASE>.html`
  - PDF: [tint](https://github.com/eddelbuettel/tint)

## License

[![](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/)
